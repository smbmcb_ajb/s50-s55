import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import AppNavbar from '../components/AppNavbar';



export default function Home(){
	
	return(
		<>
		
			<Banner/>
        	<Highlights/>
		</>
	)
}